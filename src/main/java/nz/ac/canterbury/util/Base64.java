package nz.ac.canterbury.util;

public class Base64 {
    private static final int sFillChar = 61;
    private static final String cvt = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    public static String encode(byte[] data) {
        int len = data.length;
        StringBuilder ret = new StringBuilder((len / 3 + 1) * 4);
        for (int i = 0; i < len; i++) {
            int c = data[i] >> 2 & 0x3F;
            ret.append(cvt.charAt(c));
            c = data[i] << 4 & 0x3F;
            i++;
            if (i < len) {
                c |= data[i] >> 4 & 0xF;
            }
            ret.append(cvt.charAt(c));
            if (i < len) {
                c = data[i] << 2 & 0x3F;
                i++;
                if (i < len) {
                    c |= data[i] >> 6 & 0x3;
                }
                ret.append(cvt.charAt(c));
            } else {
                i++;
                ret.append('=');
            }
            if (i < len) {
                c = data[i] & 0x3F;
                ret.append(cvt.charAt(c));
            } else {
                ret.append('=');
            }
        }
        return ret.toString();
    }

    public static byte[] decode(String sData) {
        byte[] data = toByteArray(sData);

        int len = data.length;
        StringBuilder ret = new StringBuilder(len * 3 / 4);
        for (int i = 0; i < len; i++) {
            int c = cvt.indexOf(data[i]);
            i++;
            int c1 = cvt.indexOf(data[i]);
            c = c << 2 | c1 >> 4 & 0x3;
            ret.append((char) c);
            i++;
            if (i < len) {
                c = data[i];
                if (sFillChar == c) {
                    break;
                }
                c = cvt.indexOf((char) c);
                c1 = c1 << 4 & 0xF0 | c >> 2 & 0xF;
                ret.append((char) c1);
            }
            i++;
            if (i < len) {
                c1 = data[i];
                if (sFillChar == c1) {
                    break;
                }
                c1 = cvt.indexOf((char) c1);
                c = c << 6 & 0xC0 | c1;
                ret.append((char) c);
            }
        }
        return toByteArray(ret.toString());
    }

    private static byte[] toByteArray(String str) {
        byte[] b = new byte[str.length()];
        for (int i = 0; i < b.length; i++) {
            b[i] = ((byte) str.charAt(i));
        }
        return b;
    }
}
