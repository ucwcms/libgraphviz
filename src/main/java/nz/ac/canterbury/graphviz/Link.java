/*
 * ***********************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.graphviz;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author davidm
 */
public class Link {
    private final Node fromNode;
    private final Map<String,String> attribs = new HashMap<>();
    private Node toNode;

    public final static String LINK_STYLE_BOLD = "bold";
    public final static String LINK_STYLE_DOTTED = "dotted";

    Link(Node fromNode) {
        this.fromNode = fromNode;
    }

    public Node toNode(Node toNode) {
        this.toNode = toNode;
        return fromNode;
    }

    public Link label(String value) {
        attribs.put("label", value);
        return this;
    }
    public Link colour(int r, int g, int b) {
        attribs.put("color", String.format("%.1f %.1f %.1f", (float)r/255, (float)g/255, (float)b/255));
        return this;
    }
    // http://www.graphviz.org/doc/info/colors.html
    public Link colour(String colourName) {
        attribs.put("color", colourName);
        return this;
    }
    public Link style(String style) {
        attribs.put("style", style);
        return this;
    }

    String toDOT() {
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String, String> e : attribs.entrySet()) {
            sb.append(String.format(",%s=\"%s\"", e.getKey(), e.getValue()));
        }
        String ret = sb.toString();
        if(ret.length() > 1) {
            ret = ret.substring(1);
            return fromNode.getId() +" -> " + toNode.getId() + " [" + ret + "];";
        }
        return fromNode.getId() +" -> " + toNode.getId() + ";";
    }
}
