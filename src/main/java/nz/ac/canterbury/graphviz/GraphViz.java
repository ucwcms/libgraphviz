/*
 * ***********************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.graphviz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * <dl>
 * <dt>Purpose: GraphViz Java API
 * <dd>
 *
 * <dt>Description:
 * <dd> With this Java class you can simply call dot from your Java programs.
 * <dt>Example usage:
 * <dd>
 *     <pre></pre>
 * </dd>
 *
 * </dl>
 *
 * @version v0.8.0, 2019/09/23 (September) -- Overhaul. Cleanup code flow. Bug fixes, Added better exception thorwing and removal of null and int returns on error. David Murch
 * @version v0.5.1, 2013/03/18 (March) -- Patch of Juan Hoyos (Mac support)
 * @version v0.5, 2012/04/24 (April) -- Patch of Abdur Rahman (OS detection +
 * start subgraph + read config file)
 * @version v0.4, 2011/02/05 (February) -- Patch of Keheliya Gallaba is added.
 * Now you can specify the type of the output file: gif, dot, fig, pdf, ps, svg,
 * png, etc.
 * @version v0.3, 2010/11/29 (November) -- Windows support + ability to read the
 * graph from a text file
 * @version v0.2, 2010/07/22 (July) -- bug fix
 * @version v0.1, 2003/12/04 (December) -- first release
 * @author David Murch <david@wetstone.co.nz>
 */
class GraphViz {

    private final String pathToDOT;
    /**
     * The image size in dpi. 96 dpi is normal size. Higher values are 10%
     * higher each. Lower values 10% lower each.
     *
     * dpi patch by Peter Mueller
     */
    private final int[] dpiSizes = {46, 51, 57, 63, 70, 78, 86, 96, 106, 116, 128, 141, 155, 170, 187, 206, 226, 249};
    /**
     * Define the index in the image size array.
     */
    private int currentDpiPos = 15;
    //The source of the graph written in dot language.
    private StringBuilder graph = new StringBuilder();
    //Path to directory to create the dot source and image output files
    private final String tempDir;

    /**
     * Constructor taking the dot path and temp dir
     * @param pathToDOT path to DOT binary on system
     * @param tempDir path to temp dir
     */
    GraphViz(String pathToDOT, String tempDir) {
        this.pathToDOT = pathToDOT;
        this.tempDir = tempDir;
    }

    /**
     * Increase the image size (dpi).
     */
    @SuppressWarnings("unused")
    void increaseDpi() {
        if (this.currentDpiPos < (this.dpiSizes.length - 1)) {
            ++this.currentDpiPos;
        }
    }

    /**
     * Decrease the image size (dpi).
     */
    @SuppressWarnings("unused")
    void decreaseDpi() {
        if (this.currentDpiPos > 0) {
            --this.currentDpiPos;
        }
    }

    /**
     * Get the current set DPI
     * @return the DPI
     */
    @SuppressWarnings("unused")
    int getImageDpi() {
        return this.dpiSizes[this.currentDpiPos];
    }

    /**
     * Returns the graph's source description in dot language.
     *
     * @return Source of the graph in dot language.
     */
    String getDotSource() {
        return this.graph.toString();
    }

    /**
     * Adds a string to the graph's source (without newline).
     *
     * @param line the dot string to output on a line
     */
    @SuppressWarnings("unused")
    void add(String line) {
        this.graph.append(line);
    }

    /**
     * Adds a string to the graph's source (with newline).
     *
     * @param line add a DOT line of text
     */
    void addln(String line) {
        this.graph.append(line).append("\n");
    }
    
    /**
     * Adds a newline to the graph's source.
     */
    @SuppressWarnings("unused")
    void addln() {
        this.graph.append('\n');
    }

    /**
     * Reset the graph back to blank
     */
    void clearGraph() {
        this.graph = new StringBuilder();
    }

    /**
     * Returns the graph as an image in binary format.
     *
     * @param dotSource Source of the graph to be drawn.
     * @param type Type of the output image to be produced, e.g.: gif, dot, fig,
     * pdf, ps, svg, png.
     * @return A byte array containing the image of the graph.
     * @exception GraphVizException if there is an error with IO
     */
    byte[] getGraph(String dotSource, String type) throws GraphVizException {
        try {
            File dot = writeDotSourceToFile(dotSource);
            byte[] imgStream = getImgStream(dot, type);
            if (!dot.delete()) {
                System.err.println("Warning: " + dot.getAbsolutePath() + " could not be deleted!");
            }
            return imgStream;
        } catch (java.io.IOException ioe) {
            throw new GraphVizException(ioe.getMessage(), ioe);
        }
    }

    /**
     * Writes the graph's image in a file.
     *
     * @param img A byte array containing the image of the graph.
     * @param file Name of the file to where we want to write.
     */
    @SuppressWarnings("unused")
    void writeGraphToFile(byte[] img, String file) throws GraphVizException {
        File to = new File(file);
        writeGraphToFile(img, to);
    }

    /**
     * Writes the graph's image in a file.
     *
     * @param img A byte array containing the image of the graph.
     * @param to A File object to where we want to write.
     * @exception GraphVizException if error with IO
     */
    void writeGraphToFile(byte[] img, File to) throws GraphVizException {
        try (FileOutputStream fos = new FileOutputStream(to)) {
            fos.write(img);
        } catch (IOException ex) {
            throw new GraphVizException(ex.getMessage(), ex);
        }
    }

    /**
     * It will call the external dot program and create the image output from the DOT source file.
     * The created image will be read and returned as a byte array.
     *
     * @param dot Source of the graph (in dot language).
     * @param type Type of the output image to be produced, e.g.: gif, dot, fig,
     * pdf, ps, svg, png.
     * @return The image of the graph as bytes.
     * @exception GraphVizException If there is an issue with the creating of the image from source
     */
    private byte[] getImgStream(File dot, String type) throws GraphVizException {
        File img;
        byte[] imgStream;

        try {
            img = File.createTempFile("graph_", "." + type, new File(tempDir));
            Runtime rt = Runtime.getRuntime();

            // patch by Mike Chenault
            String[] args = {pathToDOT, "-T" + type, "-Gdpi=" + dpiSizes[this.currentDpiPos], dot.getAbsolutePath(), "-o", img.getAbsolutePath()};
            Process p = rt.exec(args);
            p.waitFor();

            try (FileInputStream in = new FileInputStream(img.getAbsolutePath())) {
                imgStream = new byte[in.available()];
                int numberOfBytesRead = in.read(imgStream);
                if(numberOfBytesRead != imgStream.length) {
                    System.err.println("Warning: Read less than buffer. GraphViz read "+numberOfBytesRead+" from image. Steam buffer size was "+imgStream.length);
                }
            }

            if (!img.delete()) {
                System.err.println("Warning: " + img.getAbsolutePath() + " could not be deleted!");
            }
        } catch (java.io.IOException ioe) {
            System.err.println("Error:    in I/O processing of tempfile in dir " + tempDir + "\n");
            System.err.println("       or in calling external command");
            System.err.println("DOT source file "+dot.getAbsolutePath());
            throw new GraphVizException(ioe.getMessage(), ioe);
        } catch (java.lang.InterruptedException ie) {
            System.err.println("Error: the execution of the external program was interrupted");
            throw new GraphVizException(ie.getMessage(), ie);
        }

        return imgStream;
    }

    /**
     * Writes the source of the graph in a file, and returns the written file as
     * a File object.
     *
     * @param dotStr Source of the graph (in dot language).
     * @return The file (as a File object) that contains the source of the
     * graph.
     * @exception IOException If there is an error with IO
     */
    private File writeDotSourceToFile(String dotStr) throws IOException {
        try {
            File temp = File.createTempFile("dorrr", ".dot", new File(tempDir));
            try (BufferedWriter br = new BufferedWriter(new FileWriter(temp))) {
                br.write(dotStr);
                br.flush();
            }
            return temp;
        } catch (IOException e) {
            System.err.println("Error: I/O error while writing the dot source to temp file! " + tempDir);
            throw e;
        }
    }

    /**
     * Returns a string that is used to start a graph.
     *
     * @return A string to open a graph.
     */
    String startGraph() {
        return "digraph G {";
    }

    /**
     * Returns a string that is used to end a graph.
     *
     * @return A string to close a graph.
     */
    String endGraph() {
        return "}";
    }

    /**
     * Takes the cluster or subgraph id as input parameter and returns a string
     * that is used to start a subgraph.
     *
     * @param clusterid id of subgraph
     * @return A string to open a subgraph.
     */
    @SuppressWarnings("unused")
    String startSubgraph(int clusterid) {
        return "subgraph cluster_" + clusterid + " {";
    }

    /**
     * Returns a string that is used to end a graph.
     *
     * @return A string to close a graph.
     */
    @SuppressWarnings("unused")
    String endSubgraph() {
        return "}";
    }

    /**
     * Read a DOT graph from a text file.
     *
     * @param inputFile Input text file containing the DOT graph source.
     * @exception GraphVizException if error with IO
     */
    @SuppressWarnings("unused")
    void readSource(String inputFile) throws GraphVizException {
        StringBuilder sb = new StringBuilder();

        try {
            FileInputStream fis = new FileInputStream(inputFile);
            try (DataInputStream dis = new DataInputStream(fis)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(dis));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
            }
            this.graph = sb;
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            throw new GraphVizException(e.getMessage(), e);
        }


    }
    
}
