/*
 * ***********************************************************************
 *
 * University of Canterbury
 * __________________
 *
 *  [2013] - [2019] University of Canterbury
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of University of Canterbury and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to University of Canterbury
 * and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from  the University of Canterbury.
 */
package nz.ac.canterbury.graphviz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author davidm
 */
public class Node {
    public final static String NODE_SHAPE_BOX = "box";
    public final static String NODE_STYLE_FILLED = "filled";
    
    private final Map<String,String> attribs = new HashMap<>();
    private final String id;
    private final List<Link> links = new ArrayList<>();

    
    Node(String id) {
        this.id = id;
    }
    
    public Node shape(String shape) {
        attribs.put("shape", shape);
        return this;
    }
    public Node label(String value) {
        attribs.put("label", value);
        return this;
    }
    public Node colour(int r, int g, int b) {
        attribs.put("color", String.format("%.1f %.1f %.1f", (float)r/255, (float)g/255, (float)b/255));
        return this;
    }
    // http://www.graphviz.org/doc/info/colors.html
    public Node colour(String colourName) {
        attribs.put("color", colourName);
        return this;
    }
    public Node style(String style) {
        attribs.put("style", style);
        return this;
    }

    String getId() {
        return id;
    }

    String toDOT() {
        StringBuilder sb = new StringBuilder();
        for(Entry<String, String> e : attribs.entrySet()) {
            sb.append(String.format(",%s=\"%s\"", e.getKey(), e.getValue()));
        }
        String ret = sb.toString();
        if(ret.length() > 1) {
            ret = ret.substring(1);
            return id + " [" + ret + "];";
        }
        return "";
    }

    List<Link> getLinks() {
        return links;
    }

    public Link createLink() {
        Link link = new Link(this);
        links.add(link);
        return link;
    }
}
