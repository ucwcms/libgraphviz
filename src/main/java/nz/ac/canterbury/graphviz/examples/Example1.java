/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.canterbury.graphviz.examples;

import nz.ac.canterbury.graphviz.Diagram;
import nz.ac.canterbury.graphviz.GraphVizException;
import nz.ac.canterbury.graphviz.Link;
import nz.ac.canterbury.graphviz.Node;

/**
 *
 * @author davidm
 */
public class Example1 {
    public static void main(String[] args) {
        Diagram d = new Diagram();
        d.createNode("main")
                .shape("box")
                .createLink().toNode(d.createNode("cleanup"))
                .createLink().toNode(d.createNode("parse").createLink().toNode(d.createNode("execute")))
                .createLink()
                .colour("red")
                .style(Link.LINK_STYLE_BOLD)
                .label("100 times")
                .toNode(d.createNode("printf"))
                .createLink().style(Link.LINK_STYLE_DOTTED).toNode(d.createNode("init").createLink().toNode(d.createNode("make").label("make a string")));
        d.getNode("execute").createLink().toNode(d.getNode("make"));
        d.getNode("execute").createLink().toNode(d.getNode("printf"));
        d.getNode("execute").createLink()
                .colour("red")
                .toNode(d.createNode("compare")
                        .shape(Node.NODE_SHAPE_BOX)
                        .style(Node.NODE_STYLE_FILLED)
                        .colour("mediumpurple1"));

        try {
            d.outputToImageFile("graph-output.png");
            System.out.println("<img src=\"" + d.encodeToDataImageURI() + "\"/>");
        } catch (GraphVizException ex) {
            ex.printStackTrace();
        }
    }
}
