/*
 * ***********************************************************************
 *
 * University of Canterbury __________________
 *
 * [2013] - [2019] University of Canterbury All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of
 * University of Canterbury and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to University of
 * Canterbury and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from the
 * University of Canterbury.
 */
package nz.ac.canterbury.graphviz;

public class GraphVizException extends Exception {
    public GraphVizException() {
        super();
    }

    public GraphVizException(String message) {
        super(message);
    }

    public GraphVizException(String message, Throwable cause) {
        super(message, cause);
    }
}
