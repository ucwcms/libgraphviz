/*
 * ***********************************************************************
 *
 * University of Canterbury __________________
 *
 * [2013] - [2019] University of Canterbury All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of
 * University of Canterbury and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to University of
 * Canterbury and its suppliers and may be covered by local and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from the
 * University of Canterbury.
 */
package nz.ac.canterbury.graphviz;

import nz.ac.canterbury.util.Base64;

import java.io.File;
import java.util.*;

public class Diagram {
    private final GraphViz gv;
    private final HashMap<String, Node> nodes = new HashMap<>();
    private volatile boolean isRendered = false;

    public Diagram(String pathToDOT, String tempDir) {
        gv = new GraphViz(pathToDOT, tempDir);
    }

    public Diagram() {
        gv = new GraphViz("/usr/bin/dot", "/tmp");
    }

    private void start() {
        gv.addln(gv.startGraph());
    }

    private void end() {
        gv.addln(gv.endGraph());
        gv.increaseDpi();

    }

    public Node createNode(String id) {
        Node node = new Node(id);
        nodes.put(id, node);
        return node;
    }

    public Node getNode(String id) {
        return nodes.get(id);
    }

    private synchronized void render() {
        if(isRendered) return;
        gv.clearGraph();
        start();
        for(Node n : nodes.values()) {
            String nodeStr = n.toDOT();
            if(nodeStr.length() > 0) {
                gv.addln(nodeStr);
            }
            for(Link l : n.getLinks()) {
                gv.addln(l.toDOT());
            }
        }
        end();
        isRendered = true;
    }

    /**
     * Transform the source to an image and write to the provided image file.
     * @param path Path of where to save the image included file and file extention. e.g. /home/name/diagram.png
     */
    public void outputToImageFile(String path) throws GraphVizException {
        if(!isRendered) render();
        if(path == null || path.isEmpty()) {
            return;
        }
        File out = new File(path);
        gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), path.substring(path.lastIndexOf(".")+1)), out);
    }

    private String encodeToBase64() throws GraphVizException {
        return Base64.encode(gv.getGraph(gv.getDotSource(), "png"));
    }

    public String encodeToDataImageURI() throws GraphVizException {
        if(!isRendered) render();
        return "data:image/png;base64,"+encodeToBase64();
    }
}
