# Java Library for GraphViz

Java API for creating a chart using the DOT language. This library uses wrappers around DOT command. DOT will need to be on your system before this library will function.

A path to DOT can be provided from the client.

# Setup

`$ apt-get install graphviz ttf-freefont`


# Build

`$ mvn clean install`

# Example

```Java

Diagram d = new Diagram();
        d.createNode("main")
                .shape("box")
                .createLink().toNode(d.createNode("cleanup"))
                .createLink().toNode(d.createNode("parse").createLink().toNode(d.createNode("execute")))
                .createLink()
                .colour("red")
                .style(Link.LINK_STYLE_BOLD)
                .label("100 times")
                .toNode(d.createNode("printf"))
                .createLink().style(Link.LINK_STYLE_DOTTED).toNode(d.createNode("init").createLink().toNode(d.createNode("make").label("make a string")));
        d.getNode("execute").createLink().toNode(d.getNode("make"));
        d.getNode("execute").createLink().toNode(d.getNode("printf"));
        d.getNode("execute").createLink()
                .colour("red")
                .toNode(d.createNode("compare")
                        .shape(Node.NODE_SHAPE_BOX)
                        .style(Node.NODE_STYLE_FILLED)
                        .colour("mediumpurple1"));

        try {
            d.outputToImageFile("graph-output.png");
            System.out.println("<img src=\"" + d.encodeToDataImageURI() + "\"/>");
        } catch (GraphVizException ex) {
            ex.printStackTrace();
        }

```

